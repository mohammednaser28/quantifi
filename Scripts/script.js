///////////////////////////////////////////////////////////
// Set current year
console.log("FF")
const yearEl = document.querySelector(".year");
const currentYear = new Date().getFullYear();
yearEl.textContent = currentYear;
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
// Make mobile navigation work

const navButton = document.querySelector(".btn-mobile-nav");
const headerElement = document.querySelector(".header-section");

navButton.addEventListener("click", function () {
  headerElement.classList.toggle("nav-open");
});
///////////////////////////////////////////////////////////
// smooth scrolling animation

const id = document.querySelectorAll("a:link");
id.forEach(function (link) {
  link.addEventListener("click", function (e) {
    // e.preventDefault();
    const href = link.getAttribute("href");
    console.log(href);
    //scroll back to top
    if (href === "#")
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    // SCroll to other linke
    if (href !== "#" && href.startsWith("#")) {
      const SectionEl = document.querySelector(href);
      SectionEl.scrollIntoView({ behavior: "smooth" });
    }
    // close nav
    // if (link.classList.contains("nav-header"))
    //   headerEl.classList.toggle("nav-open");
        // Close navigation if the link has the "nav-header" class
        if (link.classList.contains("main-nav-link")) {
          const headerEl = document.getElementById("headerEl"); // Assuming you have a header element with this ID
          if (headerEl) {
            headerEl.classList.toggle("nav-open");
          }
        }
  });
});
//////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
// Fixing flexbox gap property missing in some Safari versions
function checkFlexGap() {
    var flex = document.createElement("div");
    flex.style.display = "flex";
    flex.style.flexDirection = "column";
    flex.style.rowGap = "1px";

    flex.appendChild(document.createElement("div"));
    flex.appendChild(document.createElement("div"));

    document.body.appendChild(flex);
    var isSupported = flex.scrollHeight === 1;
    flex.parentNode.removeChild(flex);
    console.log(isSupported);

    if (!isSupported) document.body.classList.add("no-flexbox-gap");
}
checkFlexGap();
